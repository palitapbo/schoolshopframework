/*
USE [master]
GO
CREATE LOGIN [apiserver_schoolshop] WITH PASSWORD=N'Sch00lSh0p!@#', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
*/

--drop DATABASE [SchoolShop]

CREATE DATABASE [SchoolShop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SchoolShop', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS01\MSSQL\DATA\SchoolShop.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SchoolShop_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS01\MSSQL\DATA\SchoolShop_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SchoolShop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SchoolShop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SchoolShop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SchoolShop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SchoolShop] SET ARITHABORT OFF 
GO
ALTER DATABASE [SchoolShop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SchoolShop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SchoolShop] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [SchoolShop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SchoolShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SchoolShop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SchoolShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SchoolShop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SchoolShop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SchoolShop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SchoolShop] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SchoolShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SchoolShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SchoolShop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SchoolShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SchoolShop] SET  READ_WRITE 
GO
ALTER DATABASE [SchoolShop] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SchoolShop] SET  MULTI_USER 
GO
ALTER DATABASE [SchoolShop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SchoolShop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SchoolShop] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SchoolShop]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [SchoolShop] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO
USE [SchoolShop]
GO
CREATE ROLE [execRole]
GO
use [SchoolShop]
GO
GRANT CONNECT TO [execRole]
GO
use [SchoolShop]
GO
GRANT EXECUTE TO [execRole]
GO
USE [SchoolShop]
GO
CREATE USER [apiserver_schoolshop] FOR LOGIN [apiserver_schoolshop]
GO
USE [SchoolShop]
GO
ALTER ROLE [execRole] ADD MEMBER [apiserver_schoolshop]
GO

USE [SchoolShop]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[School]( --s�ownik szk�
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CREATED] [datetime] NOT NULL CONSTRAINT [DF_School_CREATED]  DEFAULT (getdate()),
	[MODIFIED] [datetime] NOT NULL CONSTRAINT [DF_School_MODIFIED]  DEFAULT (getdate()),
 CONSTRAINT [PK_School_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[School_ID] int NOT NULL,
	[UniqueID] [varchar](100) NOT NULL,
	[Password] [varbinary](200) NOT NULL,
	[Enabled] bit NOT NULL CONSTRAINT [DF_User_Enabled]  DEFAULT (1),
	[UserType] int NOT NULL, --0-admin; 1-dystrybucja/sprzeda� cegie�ek; 2-sprzeda� w sklepiku
	[CREATED] [datetime] NOT NULL CONSTRAINT [DF_User_CREATED]  DEFAULT (getdate()),
	[MODIFIED] [datetime] NOT NULL CONSTRAINT [DF_User_MODIFIED]  DEFAULT (getdate()),
 CONSTRAINT [PK_User_ID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [User_FK_School] FOREIGN KEY([School_ID])
REFERENCES [dbo].[School] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [User_FK_School]
GO
CREATE UNIQUE INDEX [IDX_User_UniqueID] ON [dbo].[User]
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[UserToken](
	[Token] nvarchar(50) NOT NULL,
	[User_ID] INT NOT NULL,
	[ValidTo] datetime NOT NULL,
	[CREATED] [datetime] NOT NULL CONSTRAINT [DF_UserToken_CREATED]  DEFAULT (getdate()),
 CONSTRAINT [PK_UserToken_Token] PRIMARY KEY CLUSTERED 
(
	[Token] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserToken]  WITH CHECK ADD  CONSTRAINT [UserToken_FK_User] FOREIGN KEY([User_ID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[UserToken] CHECK CONSTRAINT [UserToken_FK_User]
GO

CREATE TABLE [dbo].[Card](
	[UniqueID] [varchar](100) NOT NULL,
	[Enabled] bit NOT NULL CONSTRAINT [DF_Card_Enabled]  DEFAULT (1),
	[CREATED] [datetime] NOT NULL CONSTRAINT [DF_Card_CREATED]  DEFAULT (getdate()),
	[MODIFIED] [datetime] NOT NULL CONSTRAINT [DF_Card_MODIFIED]  DEFAULT (getdate()),
 CONSTRAINT [PK_Card_UniqueID] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Coupon](
	[UniqueID] [varchar](100) NOT NULL,
	[School_ID] int NOT NULL,
	[Card_UniqueID] [varchar](100) NULL,
	[SoldUser_ID] INT NULL,
	[SoldDate] [datetime] NULL,
	[CheckedUser_ID] INT NULL,
	[CheckedDate] [datetime] NULL,
	[ValidTo] datetime NOT NULL,
	[CREATED] [datetime] NOT NULL CONSTRAINT [DF_Coupon_CREATED]  DEFAULT (getdate()),
 CONSTRAINT [PK_Coupon_UniqueID] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Coupon]  WITH CHECK ADD  CONSTRAINT [Coupon_FK_SoldUser] FOREIGN KEY([SoldUser_ID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Coupon] CHECK CONSTRAINT [Coupon_FK_SoldUser]
GO
ALTER TABLE [dbo].[Coupon]  WITH CHECK ADD  CONSTRAINT [Coupon_FK_CheckedUser] FOREIGN KEY([CheckedUser_ID])
REFERENCES [dbo].[User] ([ID])
GO
ALTER TABLE [dbo].[Coupon] CHECK CONSTRAINT [Coupon_FK_CheckedUser]
GO
ALTER TABLE [dbo].[Coupon]  WITH CHECK ADD  CONSTRAINT [Coupon_FK_School] FOREIGN KEY([School_ID])
REFERENCES [dbo].[School] ([ID])
GO
ALTER TABLE [dbo].[Coupon] CHECK CONSTRAINT [Coupon_FK_School]
GO
CREATE NONCLUSTERED INDEX [IDX_Coupon_School_ID] ON [dbo].[Coupon]
(
	[School_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
ALTER TABLE [dbo].[Coupon]  WITH CHECK ADD  CONSTRAINT [Coupon_FK_Card] FOREIGN KEY([Card_UniqueID])
REFERENCES [dbo].[Card] ([UniqueID])
GO
ALTER TABLE [dbo].[Coupon] CHECK CONSTRAINT [Coupon_FK_Card]
GO
CREATE NONCLUSTERED INDEX [IDX_Coupon_Card_UniqueID] ON [dbo].[Coupon]
(
	[Card_UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[UserToken_Check]
@Token nvarchar(50),
@UserID int output,
@SchoolID int output,
@UserType int output
as
begin
	set nocount on;
	set @UserID=0

	select @UserID=[User_ID]
	,@SchoolID=u.School_ID
	,@UserType=u.UserType
	from dbo.UserToken ut
	inner join dbo.[User] u
	on ut.[User_ID] = u.ID
	where Token=@token
	and ValidTo>GETDATE()
	and u.[Enabled]=1
	
	if isnull(@userID,0)=0
		set @userID=0

end
GO

CREATE procedure [dbo].[UserToken_Insert]
@UserID int,
@token nvarchar(50) output
as
begin
	set nocount on;
	set @token=NEWID() --przysz�o�ciowo: token mo�e zawiera� School_ID, UserType, User_ID - mo�na upro�ci� i przyspieszy� dzia�anie UserToken_Check (bez zapytania do bazy!)

	insert into dbo.UserToken (Token, [User_ID], ValidTo)
	values (@token, @UserID, dateadd(hh,12,GETDATE()) ); --tokeny wa�ne przez 12h

	if @@error<>0
	begin
		set @token=''
	end
	
	if @token<>''
	begin
		delete from dbo.UserToken where [User_ID]=@UserID and Token<>@token
	end

end
GO

CREATE procedure [dbo].[User_Login]
@UniqueID [varchar](100),
@Password [varchar](200),
@UserID int output,
@token nvarchar(50) output
as
begin
	set nocount on;
	set @userID=0

	SELECT @userID=ID
	FROM dbo.[User]
	WHERE [UniqueID]=@UniqueID
	and [Password]=cast(@Password as varbinary)
	--and [Password]=HASHBYTES('MD5',@Password)

	if isnull(@userID,0)=0
		set @userID=0;
		
	if isnull(@userID,0)<>0 and (select [enabled] from dbo.[User] where ID=@userID) = 0 --user disabled
		set @userID=-2;

	if isnull(@UniqueID,'')='' --UniqueID missing
		set @userID=-1;

	set @token='';

	if @userID>0
	begin
		exec [dbo].[UserToken_Insert] @userID, @token output
	end

end
GO

CREATE procedure [dbo].[Coupon_Check]
@UniqueID [varchar](100),
@SchoolID [varbinary](200),
@UserType int,
@UserID int,
@result int output
as
begin
	set nocount on;
	set @result=0
	
	if @UserType not in (0,2)
		set @result=-1; --user not allowed to check coupons

	if @result=0 and not exists (select * from dbo.Coupon where UniqueID=@UniqueID and School_ID=@SchoolID)
		set @result=-2; --no such coupon for school
	
	if @result=0 and (select SoldDate from dbo.Coupon where UniqueID=@UniqueID) is null
		set @result=-3 --coupon not sold

	if @result=0 and (select CheckedDate from dbo.Coupon where UniqueID=@UniqueID) is not null
		set @result=-4 --coupon already used

	if @result=0 and (select ValidTo from dbo.Coupon where UniqueID=@UniqueID)<getdate()
		set @result=-5 --coupon not valid

	if @result=0
	begin
		update dbo.Coupon set CheckedUser_ID=@UserID, CheckedDate=getdate() where UniqueID=@UniqueID
	end

end
GO


