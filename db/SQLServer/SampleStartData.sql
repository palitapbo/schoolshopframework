USE [SchoolShop]
GO
insert into dbo.School (Name)
values ('Test')
GO
--select * from dbo.school
insert into [dbo].[User] (School_ID, UniqueID, Password, Enabled, UserType)
values (1,'abcdef123456',cast('abc123' as varbinary),1,0)
GO
--select * from dbo.[user]
insert into dbo.Coupon (UniqueID, School_ID, Card_UniqueID, SoldUser_ID, SoldDate, CheckedUser_ID, CheckedDate, ValidTo)
values ('201912010001123',1,NULL,1,'2019-12-01',NULL,NULL,'2020-12-31')
GO
--select * from dbo.Coupon