﻿<?php

defined('SCHOOLSHOP_APP') OR exit('No direct script access allowed');

/**
 * WEB service SchoolShopApp - class User
 *
 */
class User {
	/**
	 * @var Database $database
	 */
	private $database = null;
	
	
	public function Login(&$response_code) {
		$req_headers = apache_request_headers();
		$uniqueID="";
		if (!empty($req_headers["Uniqueid"])) {
			$uniqueID = $req_headers["Uniqueid"];
		}
		$password="";
		if (!empty($req_headers["Password"])) {
			$password = $req_headers["Password"];
		}
		$result = $this->database->User_Login($uniqueID,$password,$response_code);
		return $result;
	}
	
	
	public function __construct() {
		$this->database = $GLOBALS["database"];
	}

}

?>