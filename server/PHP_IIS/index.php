﻿<?php

/**
 * WEB service SchoolShopApp
*/

define("SCHOOLSHOP_APP", 1);

require_once 'database.php';

$method = "";
$urlapp = filter_input(INPUT_GET, "app");
$method = filter_input(INPUT_GET, "method");
$response_code = 404;
$response = "";
$schoolID = -1;
$userType = -1;
$database = new Database();

$reqmethod = $_SERVER['REQUEST_METHOD'];

switch ($urlapp) {
	case "User":
		require_once 'user.php';
		   //@var $application User
		$application = new User();
		if ($method=="Login") {
				switch ($reqmethod) {
				case "GET":
					$response = $application->Login($response_code);
					break;
				}
				break;
		}
		break;
	case "Coupon":
		require_once 'coupon.php';
		   //@var $application Coupon
		$application = new Coupon();
		if ($method=="Check") {
			switch ($reqmethod) {
				case "PUT":
					$response = $application->Check($response_code,$schoolID,$userType);
					break;
			}
		}
		/** if ($method=="Sell") {
			switch ($reqmethod) {
				case "PUT":
					$response = $application->Sell($response_code); //sprzedaż 1 kuponu
					break;
			}
		} */
		break;
	/** case "Card":
		require_once 'card.php';
		   //@var $application Card
		$application = new Card();
		if ($method=="") {
			switch ($reqmethod) {
				case "GET":
					$response = $application->Get($response_code); //liczba kuponów na karcie
					break;
				case "POST":
					$response = $application->Post($response_code); //nowa karta
					break;
				case "PUT":
					$response = $application->Put($response_code); //użycie kuponu z karty
					break;
			}
		}
		if ($method=="Sell") {
			switch ($reqmethod) {
				case "PUT":
					$response = $application->Sell($response_code); //sprzedaż kuponu na kartę
					break;
			}
		}
		break; */
	case "ServerTime":
		if ($method=="") {
			switch ($reqmethod) {
				case "GET":
					/*
					 Get
					  --zwraca czas w UTC
					 Przykład odpowiedzi: 
					{
						"result": 
							{
								"utc": 1544985948
							}
					}
					*/
					$response_code = 200;
					$utc = time();
					$response = "{\"result\":{\"utc\":"
						. json_encode($utc) . "}}";
					break;
			}
		}
		break;
}

//echo '{"url":"'.$urlapp.'","method":"'.$method.'"}';

header("Content-Type: application/json");
http_response_code($response_code);
if (empty($response)) {
	$response = '{"Status":{"Description":"Unknown error - index","Code":498}}';
}
echo $response;

?>
