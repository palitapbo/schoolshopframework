﻿<?php

defined('SCHOOLSHOP_APP') OR exit('No direct script access allowed');

/**
 * WEB service SchoolShopApp - class Coupon
 *
 */
class Coupon {
	/**
	 * @var Database $database
	 */
	private $database = null;
	
	public function Check(&$response_code, &$schoolID, &$userType) {
		$req_headers = apache_request_headers();
		$token = "";
		if (!empty($req_headers["Token"])) {
			$token = $req_headers["Token"];
		}
		$uniqueID = filter_input(INPUT_GET, "Uniqueid");
		$userID = -1;
		$schoolID = -1;
		$userType = -1;
		$userID = $this->database->UserToken_Check($token, $schoolID, $userType);
		if ($userID > 0) {
			$result = $this->database->Coupon_Check($uniqueID,$schoolID,$userType,$userID,$response_code);
			return $result;
		}
		if ($userID == 0) {
			$response_code = 403;
			$result = '{"Status":{"Description":"Token invalid","Code":401}}';
			return $result;
			}
		if ($userID == -1) {
			$response_code = 403;
			$result = '{"Status":{"Description":"Unknown error","Code":499}}';
			return $result;
		}
	}
	
	
	public function __construct() {
		$this->database = $GLOBALS["database"];
	}

}

?>