<?php

defined('SCHOOLSHOP_APP') OR exit('No direct script access allowed');

/**
 * WEB service SchoolShopApp - class Database
 *
 */

class Database {

	const SQL_SERVER_NAME = "MSERVER\SQLEXPRESS01";
	const DATABASE_NAME = "SchoolShop";

	private $connection = false;
	
	/**
	 * Token Check
	 * 
	 * @return UserID, SchoolID and UserType
	 */
	public function UserToken_Check($token, &$schoolID, &$userType) {
		if ($this->connect()) {
			$userID = -1;
			$stored_proc = "{call dbo.UserToken_Check(?,?,?,?)}";
			$stored_proc_params = array(
				array($token, SQLSRV_PARAM_IN)
				, array(&$userID, SQLSRV_PARAM_OUT)
				, array(&$schoolID, SQLSRV_PARAM_OUT)
				, array(&$userType, SQLSRV_PARAM_OUT)
			);
			$qry = sqlsrv_query($this->connection, $stored_proc, $stored_proc_params);
			return $userID;
		} else {
			return -1;
		}
	}

	public function User_Login ($uniqueID, $password, &$response_code) {
		if ($this->connect()) {
			$stored_proc = "{call dbo.User_Login(?,?,?,?)}";
			$userID = -3;
			$token = "";
			$stored_proc_params = array(
				array($uniqueID, SQLSRV_PARAM_IN)
				, array($password, SQLSRV_PARAM_IN)
				, array(&$userID, SQLSRV_PARAM_OUT)
				, array(&$token, SQLSRV_PARAM_OUT)
			);
			$qry = sqlsrv_query($this->connection, $stored_proc, $stored_proc_params);
			if ($qry === false) { 
				$response_code = 403;
				return '{"Status":{"Description":"Unknown error - sql proc failed","Code":497}}';
			} 
			if ($userID > 0) {
				$response_code = 200;
				return '{"result":{"token":"'.$token.'"}}';
			}
			if ($userID == 0) {
				$response_code = 403;
				return '{"Status":{"Description":"Bad username or password","Code":410}}';
			}
			if ($userID == -1) {
				$response_code = 403;
				return '{"Status":{"Description":"UniqueID missing","Code":411}}';
			}
			if ($userID == -2) {
				$response_code = 403;
				return '{"Status":{"Description":"User disabled by admin","Code":412}}';
			}
			if ($userID == -3) {
				$response_code = 403;
				return '{"Status":{"Description":"Unknown error","Code":499}}';
			}
		} else {
			$response_code = 403;
			return '{"Status":{"Description":"Unknown error - db connect","Code":496}}';
		}
	}
	
	
	public function Coupon_Check ($UniqueID, $SchoolID, $UserType, $UserID, &$response_code) {
		if ($this->connect()) {
			$stored_proc = "{call dbo.Coupon_Check(?,?,?,?,?)}";
			$result = -6;
			$stored_proc_params = array(
				array($UniqueID, SQLSRV_PARAM_IN)
				, array($SchoolID, SQLSRV_PARAM_IN)
				, array($UserType, SQLSRV_PARAM_IN)
				, array($UserID, SQLSRV_PARAM_IN)
				, array(&$result, SQLSRV_PARAM_OUT)
			);
			$qry = sqlsrv_query($this->connection, $stored_proc, $stored_proc_params);
			if ($qry === false) { 
				$response_code = 403;
				return '{"Status":{"Description":"Unknown error - db proc failed","Code":497}}';
			} 
			if ($result == 0) {
				$response_code = 200;
				return '{}';
			}
			if ($result == -6) {
				$response_code = 403;
				return '{"Status":{"Description":"Unknown error","Code":499}}';
			}
			if ($result == -5) {
				$response_code = 403;
				return '{"Status":{"Description":"Coupon is not valid","Code":417}}';
			}
			if ($result == -4) {
				$response_code = 403;
				return '{"Status":{"Description":"Coupon already used","Code":416}}';
			}
			if ($result == -3) {
				$response_code = 403;
				return '{"Status":{"Description":"Coupon not sold","Code":415}}';
			}
			if ($result == -2) {
				$response_code = 403;
				return '{"Status":{"Description":"No such coupon for school","Code":414}}';
			}
			if ($result == -1) {
				$response_code = 403;
				return '{"Status":{"Description":"User not allowed to check coupons","Code":413}}';
			}
			if ($userID == -3) {
				$response_code = 403;
				return '{"Status":{"Description":"Unknown error","Code":499}}';
			}
		} else {
			$response_code = 403;
			return '{"Status":{"Description":"Unknown error - db connect","Code":496}}';
		}
	}
	

	//return '{"result":{"AnyList":' . $this->qryToJSON($qry) . '}}';
	
	private function qryToJSON($qry) {
		if ($qry !== false) {
			while ($row = sqlsrv_fetch_array($qry, SQLSRV_FETCH_ASSOC)) {
				$r[]=$row;
			}
			if (empty($r)) {
				return '[]';
			} else {
				//echo var_dump($r);
				//exit;
				return json_encode($r);
			}
		} else {
			return '[]';
		}
	}
	
	
	public function __destruct() {
		if ($this->connection) {
			sqlsrv_close($this->connection);
		}
	}
	
	private function connect() {
		if (!$this->connection) {
			$connectionInfo = array("Database" => Database::DATABASE_NAME, "UID" => "apiserver_schoolshop", "PWD" => "Sch00lSh0p!@#", "CharacterSet" => "UTF-8");
			$this->connection = sqlsrv_connect(Database::SQL_SERVER_NAME, $connectionInfo);
		}
		if ($this->connection) {
			return true;
		} else {
			return false;
		}
	}

}

?>